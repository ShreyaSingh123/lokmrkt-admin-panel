export const environment = {
  production: true,
  apiUrl: "https://loklmkt.azurewebsites.net/api",
  rootPathUrl: "https://loklmkt.azurewebsites.net/",
  adminDefaultPic: "./assets/dist/img/admin.jpg",
};
