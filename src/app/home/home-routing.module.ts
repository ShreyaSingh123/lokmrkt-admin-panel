import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./home.component";
import { AuthGuard } from "src/app/shared/guards/auth.guard";
import { LayoutComponent } from "src/app/layouts/layout.component";

import { AdminListComponent } from "../super-admin/admin/admin-list/admin-list.component";

import { AddAdminComponent } from "../super-admin/admin/add-admin/add-admin.component";
import { AdminDetailsComponent } from "../super-admin/admin/admin-details/admin-details.component";
import { UserListComponent } from "../super-admin/user/user-list/user-list.component";
import { UserDetailsComponent } from "../super-admin/user/user-details/user-details.component";
import { VendorListComponent } from "../super-admin/vendor/vendor-list/vendor-list.component";
import { VendorProductDetailsComponent } from "../super-admin/vendor/vendor-product-details/vendor-product-details.component";
import { AssignVendorComponent } from "../super-admin/assignToAdmin/assign-vendor/assign-vendor.component";
import { AssignUserComponent } from "../super-admin/assignToAdmin/assign-user/assign-user.component";
import { AssignProductComponent } from "../super-admin/assignToAdmin/assign-product/assign-product.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "home",
    pathMatch: "full",
  },
  {
    path: "",
    component: LayoutComponent,
    canActivate: [AuthGuard],
    children: [
      { path: "", component: HomeComponent },
      { path: "add-admin", component: AddAdminComponent },
      { path: "adminList", component: AdminListComponent },
      { path: "adminList/:id", component: AdminDetailsComponent },
      {
        path: "userList",
        component: UserListComponent,
      },
      { path: "userList/:id", component: UserDetailsComponent },
      { path: "vendorList", component: VendorListComponent },
      { path: "vendorList/:id", component: VendorProductDetailsComponent },
      { path: "assignVendor", component: AssignVendorComponent },
      { path: "assignUser", component: AssignUserComponent },
      { path: "assignProduct", component: AssignProductComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class HomeRoutingModule {}
