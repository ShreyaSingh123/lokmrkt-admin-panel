import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AdminService } from "../../admin.service";

@Component({
  selector: "app-user-list",
  templateUrl: "./user-list.component.html",
  styleUrls: ["./user-list.component.css"],
})
export class UserListComponent implements OnInit {
  adminList: Array<any>;
  constructor(private adminService: AdminService, private router: Router) {}

  ngOnInit() {
    this.userList();
  }

  userList() {
    this.adminService.userList().subscribe((res) => {
      this.adminList = res.data;
    });
  }

  deleteID(adminId) {
    this.router.navigate(["home/adminList", adminId]);
  }
}
