import { Component, OnInit, Renderer2 } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";
import { Login } from "src/app/shared/models";
import { AuthService } from "src/app/shared/services/auth.service";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ConstantValues } from "src/app/shared/enums/constant-values.enum";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loginModel: Login;
  submitted = false;
  constructor(
    private renderer: Renderer2,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private toaster: ToastrService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.renderer.addClass(document.querySelector("app-root"), "login-page");
    this.setConfigurationOfLoginForm();
    this.spinner.show();
    this.authService.logout();
    this.spinner.hide();
  }

  get f() {
    return this.loginForm.controls;
  }

  setConfigurationOfLoginForm() {
    this.loginForm = this.formBuilder.group({
      inputValue: [
        "superadmin@loklmkt.com",
        [Validators.required, Validators.email],
      ],
      password: [
        "Abs0lve@T3ch",
        [Validators.required, Validators.minLength(6)],
      ],
      deviceType: new FormControl(ConstantValues.consDeviceType),
      deviceToken: new FormControl(ConstantValues.consDeviceToken),
    });
  }

  onLogin() {
    this.spinner.show();
    this.submitted = true;
    if (this.loginForm.invalid) {
      this.spinner.hide();
      return;
    }
    this.loginModel = this.loginForm.value;
    if (
      this.loginForm.controls["inputValue"].value === "superadmin@loklmkt.com"
    ) {
      this.authService
        .superAdminLogin(this.loginModel)
        .subscribe((response) => {
          if (response.status) {
            this.spinner.hide();
            this.toaster.success(response.message);
            this.router.navigateByUrl("/home");
          }
        });
    } else if (
      this.loginForm.controls["inputValue"].value != "superadmin@loklmkt.com"
    ) {
      this.authService.adminLogin(this.loginModel).subscribe((response) => {
        localStorage.setItem("token", response.data.accessToken);
        localStorage.setItem("email", response.data.email);
        if (response.status) {
          this.spinner.hide();
          this.toaster.success(response.message);
          this.router.navigateByUrl("/home");
        } else {
          if (response.data.isEmailVerified == false) {
            this.spinner.hide();
            this.toaster.error(response.message);
            this.router.navigateByUrl("/verifyEmail");
          } else {
            this.spinner.hide();
            this.toaster.error(response.message);
          }
        }
      });
    } else {
      this.spinner.hide();
    }
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnDestroy() {
    this.renderer.removeClass(document.querySelector("app-root"), "login-page");
  }
}
