import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { MustMatch } from "src/app/shared/directives/mustMatch";
import { ConstantValues } from "src/app/shared/enums/constant-values.enum";
import { AuthService } from "src/app/shared/services/auth.service";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"],
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  urlEmail: any;
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.urlEmail = this.route.snapshot.queryParamMap.get("emailId");

    this.registerForm = this.formBuilder.group(
      {
        email: [
          this.urlEmail ? this.urlEmail : "",
          [Validators.required, Validators.email],
        ],
        password: ["", Validators.required],
        confirmPassword: ["", Validators.required],
        phoneNumber: [
          "",
          [Validators.required, Validators.pattern("^([0-9]{10}$")],
        ],
        terms: [false, Validators.requiredTrue],
        deviceType: new FormControl(ConstantValues.consDeviceType),
        deviceToken: new FormControl(ConstantValues.consDeviceToken),
      },
      {
        validator: MustMatch("password", "confirmPassword"),
      }
    );
  }

  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }
    var registrData = {
      email: this.registerForm.controls["email"].value,
      password: this.registerForm.controls["password"].value,
      phoneNumber: this.registerForm.controls["phoneNumber"].value,
      role: "admin",
      dialCode: "91",
      deviceType: this.registerForm.controls["deviceType"].value,
      deviceToken: this.registerForm.controls["deviceToken"].value,
    };
    this.spinner.show();
    localStorage.setItem("email", registrData.email);
    this.authService.register(registrData).subscribe((res) => {
      localStorage.setItem("token", res.data.accessToken);
      if (res.status) {
        this.spinner.hide();
        this.toastr.success(res.message);

        this.router.navigateByUrl("/verifyEmail");
      } else {
        this.spinner.hide();
        this.toastr.warning(res.message);
      }
    });
  }
}
