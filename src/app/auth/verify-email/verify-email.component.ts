import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { AuthService } from "src/app/shared/services/auth.service";

@Component({
  selector: "app-verify-email",
  templateUrl: "./verify-email.component.html",
  styleUrls: ["./verify-email.component.css"],
})
export class VerifyEmailComponent implements OnInit {
  isDisabled: boolean;
  email: any;
  otp: any;
  submitted: boolean;
  constructor(
    private authService: AuthService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private router: Router
  ) {}

  ngOnInit() {
    this.otpGenerate();
  }

  otpGenerate() {
    this.email = {
      inputValue: localStorage.getItem("email"),
    };
    this.spinner.show();
    this.authService.resendEmailCode(this.email).subscribe((res) => {
      if (res.status) {
        this.toastr.success(res.message);
        this.otp = res.data.otpcode;
        this.spinner.hide();
      } else {
        this.toastr.warning(res.message);
      }
    });
  }
  onOtpChange(event) {
    this.otp = event;
    if (event.length == 4) {
      this.otp = event;
      this.isDisabled = true;
    } else {
      this.isDisabled = false;
    }
  }

  resendOtp() {
    this.otpGenerate();
  }

  onSubmit() {
    this.submitted = true;
    this.spinner.show();

    // stop here if form is invalid

    var submittedData = {
      otp: this.otp,
      email: this.email.inputValue,
    };
    this.authService.verifyEmail(submittedData).subscribe((res) => {
      if (res.status) {
        this.spinner.hide();
        this.toastr.success(res.message);
        this.router.navigate(["/login"]);
      } else {
        this.spinner.hide();
        this.toastr.error(res.message);
        this.spinner.hide();
      }
    });
  }

  ngOnDestroy() {
    localStorage.removeItem("token");
    localStorage.removeItem("email");
  }
}
