import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { SuperAdminRoutingModule } from "./super-admin-routing.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AdminDetailsComponent } from "./admin/admin-details/admin-details.component";
import { AddAdminComponent } from "./admin/add-admin/add-admin.component";
import { AdminListComponent } from "./admin/admin-list/admin-list.component";
import { UserListComponent } from "./user/user-list/user-list.component";
import { UserDetailsComponent } from "./user/user-details/user-details.component";
import { VendorListComponent } from "./vendor/vendor-list/vendor-list.component";
import { VendorProductDetailsComponent } from "./vendor/vendor-product-details/vendor-product-details.component";
import { NgxPaginationModule } from "ngx-pagination";
import { AdminUserInfoComponent } from "./admin/admin-details/admin-user-info/admin-user-info.component";
import { AssignVendorComponent } from './assignToAdmin/assign-vendor/assign-vendor.component';
import { AssignUserComponent } from './assignToAdmin/assign-user/assign-user.component';
import { AssignProductComponent } from './assignToAdmin/assign-product/assign-product.component';

@NgModule({
  declarations: [
    AdminDetailsComponent,
    AddAdminComponent,
    AdminListComponent,
    UserListComponent,
    UserDetailsComponent,
    VendorListComponent,
    VendorProductDetailsComponent,
    AdminUserInfoComponent,
    AssignVendorComponent,
    AssignUserComponent,
    AssignProductComponent,
  ],
  imports: [
    CommonModule,
    SuperAdminRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
  ],
  providers: [],
})
export class SuperAdminModule {}
