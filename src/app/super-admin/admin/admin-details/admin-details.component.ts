import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, FormControl } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { environment } from "src/environments/environment";
import { SuperAdminService } from "../../super-admin.service";

@Component({
  selector: "app-admin-details",
  templateUrl: "./admin-details.component.html",
  styleUrls: ["./admin-details.component.css"],
})
export class AdminDetailsComponent implements OnInit {
  basicInfoForm: FormGroup;
  adminDetails: any;
  adminId: any;
  isLoaded = false;
  baseUrl = environment.rootPathUrl;
  profilePic: any;
  vendorsList: Array<any>;
  masterSelected: boolean;
  checkedList: Array<any> = [];
  updatedVendorList: any;
  assignVendorList: Array<any>;
  list: Array<any> = [];
  showTrash: boolean = false;
  dataShow: boolean;
  constructor(
    private superAdminService: SuperAdminService,
    private formBuilder: FormBuilder,

    private route: ActivatedRoute,
    private toasterService: ToastrService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.basicInfoForm = this.formBuilder.group({
      adminId: new FormControl({ value: "", disabled: true }),
      name: new FormControl({ value: "", disabled: true }),

      email: new FormControl({ value: "", disabled: true }),
      phoneNumber: new FormControl({ value: "", disabled: true }),
      dateOfBirth: new FormControl({ value: "", disabled: true }),
      applicationStatus: new FormControl({ value: "", disabled: true }),
    });
    this.adminId = this.route.snapshot.paramMap.get("id");

    this.getAdminDetails(this.adminId);
  }

  getAdminDetails(id: any) {
    this.spinner.show();
    this.superAdminService.getAdminDetails(id).subscribe((res) => {
      if (res.status) {
        this.adminDetails = res.data;
        this.profilePic = this.baseUrl + res.data.profilePic;

        // if (res.data.vendorsList.length === 0) {
        //   this.dataShow = true;
        // } else {
        this.vendorsList = res.data.vendorsList.filter((x) => x.isAssigned);
        // }

        if (res.data.profilePic != "") {
          this.profilePic = res.data.profilePic;
          this.spinner.hide();
        } else {
          this.profilePic = environment.adminDefaultPic;
          this.spinner.hide();
        }

        this.basicInfoForm.controls["name"].setValue(this.adminDetails.name);
        this.basicInfoForm.controls["adminId"].setValue(
          this.adminDetails.phoneNumber
        );
        this.basicInfoForm.controls["phoneNumber"].setValue(
          this.adminDetails.phoneNumber
        );
        this.basicInfoForm.controls["email"].setValue(this.adminDetails.email);
        this.basicInfoForm.controls["applicationStatus"].setValue(
          this.adminDetails.applicationStatus
        );

        this.isLoaded = true;
        // this.spinner.hide();
      } else {
        this.spinner.hide();
        this.toasterService.error(res.message);
      }
    });
  }

  // checkUncheckAll() {
  //   this.showTrash = true;
  //   for (var i = 0; i < this.vendorsList.length; i++) {
  //     this.vendorsList[i].isAssigned = this.masterSelected;
  //   }
  //   this.getCheckedItemList();
  // }
  isAllSelected() {
    this.showTrash = true;
    this.masterSelected = this.vendorsList.every(function (item: any) {
      return item.isAssigned == true;
    });
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this.checkedList = [];
    for (var i = 0; i < this.vendorsList.length; i++) {
      if (this.vendorsList[i].isAssigned)
        this.checkedList.push(this.vendorsList[i]);
    }
  }
  getVendorView(event, item) {
    this.updatedVendorList = item;
    if (this.updatedVendorList.permissions === null) {
      this.updatedVendorList.permissions = { canView: event.target.checked };
    } else {
      this.updatedVendorList.permissions.canView = event.target.checked;
    }
    this.assignVendor();
  }
  getVendorEdit(event, item) {
    this.updatedVendorList = item;
    if (this.updatedVendorList.permissions === null) {
      this.updatedVendorList.permissions = { canEdit: event.target.checked };
    } else {
      this.updatedVendorList.permissions.canEdit = event.target.checked;
    }
    this.assignVendor();
  }
  getVendorDelete(event, item) {
    this.updatedVendorList = item;
    if (this.updatedVendorList.permissions === null) {
      this.updatedVendorList.permissions = { canDelete: event.target.checked };
    } else {
      this.updatedVendorList.permissions.canDelete = event.target.checked;
      this.assignVendor();
    }
  }

  assignVendor() {
    if (this.checkedList.length === 0) {
      this.checkedList.push(this.updatedVendorList);
    } else if (this.checkedList.length != 0) {
      if (
        this.checkedList.some(
          (x) => x.vendorId === this.updatedVendorList.vendorId
        )
      ) {
        var index = this.checkedList.findIndex(
          (item) => item.vendorId === this.updatedVendorList.vendorId
        );
        this.checkedList[index] = this.updatedVendorList;
      } else {
        this.checkedList.push(this.updatedVendorList);
      }
    }
    if (this.checkedList.length != 0) {
      this.showTrash = true;
    }
  }

  // onClick() {
  //   this.showTrash = true;
  //   if (this.list.length === 0) {
  //     this.list.push(this.updatedVendorList);
  //   } else if (this.updatedVendorList.length != 0) {
  //     if (
  //       this.list.some((x) => x.vendorId === this.updatedVendorList.vendorId)
  //     ) {
  //       var index = this.list.findIndex(
  //         (item) => item.vendorId === this.updatedVendorList.vendorId
  //       );
  //     } else {
  //       this.list.push(this.updatedVendorList);
  //     }
  //   }
  // }

  assignIDs() {
    this.assignVendorList = this.checkedList.map((x) => ({
      permissions: x.permissions,

      isAssigned: x.isAssigned,
      vendorId: x.vendorId,
    }));

    var data = {
      adminId: this.adminId,
      assignPermissions: this.assignVendorList,
    };
    this.spinner.show();
    this.superAdminService.assignVendor(data).subscribe((res) => {
      if (res.status) {
        this.spinner.hide();
        this.toasterService.success(res.message);
        this.getAdminDetails(this.adminId);
        this.showTrash = false;
      } else {
        this.spinner.hide();
        this.toasterService.warning(res.message);
      }
    });
  }
}
