import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, FormControl } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { SuperAdminService } from "src/app/super-admin/super-admin.service";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-admin-user-info",
  templateUrl: "./admin-user-info.component.html",
  styleUrls: ["./admin-user-info.component.css"],
})
export class AdminUserInfoComponent implements OnInit {
  adminDetails: any;
  adminId: any;
  isLoaded = false;
  userList: Array<any>;
  masterSelected: boolean;
  checkedList: Array<any> = [];
  updatedUserList: any;
  allUserList: Array<any> = [];
  showTrash: boolean = false;
  assignUserList: any;
  constructor(
    private superAdminService: SuperAdminService,
    private formBuilder: FormBuilder,

    private route: ActivatedRoute,
    private toasterService: ToastrService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.adminId = this.route.snapshot.paramMap.get("id");

    this.getAdminDetails(this.adminId);
  }

  getAdminDetails(id: any) {
    this.spinner.show();
    this.superAdminService.getAdminDetails(id).subscribe((res) => {
      if (res.status) {
        this.adminDetails = res.data;

        this.userList = res.data.usersList.filter((x) => x.isAssigned);

        this.isLoaded = true;
      } else {
        this.spinner.hide();
        this.toasterService.error(res.message);
      }
    });
  }

  assignVendor() {
    if (this.checkedList.length === 0) {
      this.checkedList.push(this.updatedUserList);
    } else if (this.checkedList.length != 0) {
      if (
        this.checkedList.some((x) => x.userId === this.updatedUserList.vendorId)
      ) {
        var index = this.checkedList.findIndex(
          (item) => item.userId === this.updatedUserList.userId
        );
        this.checkedList[index] = this.updatedUserList;
      } else {
        this.checkedList.push(this.updatedUserList);
      }
    }

    if (this.checkedList.length != 0) {
      this.showTrash = true;
    }
  }

  getUserView(event, item) {
    this.updatedUserList = item;

    if (this.updatedUserList.permissions === null) {
      this.updatedUserList.permissions = { canView: event.target.checked };
    } else {
      this.updatedUserList.permissions.canView = event.target.checked;
    }
    this.assignVendor();
  }

  getUserEdit(event, item) {
    this.updatedUserList = item;
    if (this.updatedUserList.permissions === null) {
      this.updatedUserList.permissions = { canEdit: event.target.checked };
    } else {
      this.updatedUserList.permissions.canEdit = event.target.checked;
    }
    this.assignVendor();
  }

  getUserDelete(event, item) {
    this.updatedUserList = item;
    if (this.updatedUserList.permissions === null) {
      this.updatedUserList.permissions = { canDelete: event.target.checked };
    } else {
      this.updatedUserList.permissions.canDelete = event.target.checked;
    }
    this.assignVendor();
  }

  // checkUncheckAll() {
  //   for (var i = 0; i < this.userList.length; i++) {
  //     this.userList[i].isAssigned = this.masterSelected;
  //   }
  //   this.getCheckedItemList();
  // }
  isAllSelected() {
    this.masterSelected = this.userList.every(function (item: any) {
      return item.isAssigned == true;
    });
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this.checkedList = [];
    for (var i = 0; i < this.userList.length; i++) {
      if (this.userList[i].isAssigned) this.checkedList.push(this.userList[i]);
    }
  }

  assignIDs() {
    this.assignUserList = this.checkedList.map((x) => ({
      permissions: x.permissions,

      isAssigned: x.isAssigned,
      userId: x.userId,
    }));
    var data = {
      adminId: this.adminId,
      assignPermissions: this.assignUserList,
    };
    this.spinner.show();

    this.superAdminService.assignUser(data).subscribe((res) => {
      if (res.status) {
        this.toasterService.success(res.message);
        this.getAdminDetails(this.adminId);

        this.showTrash = false;
        this.spinner.hide();
      } else {
        this.spinner.hide();
        this.toasterService.warning(res.message);
      }
    });
  }
}
