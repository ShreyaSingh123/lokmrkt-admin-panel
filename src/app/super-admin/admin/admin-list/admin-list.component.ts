import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { Filter } from "src/app/shared/interfaces/filter";
import Swal from "sweetalert2";
declare var $;
import { SuperAdminService } from "../../super-admin.service";

@Component({
  selector: "app-admin-list",
  templateUrl: "./admin-list.component.html",
  styleUrls: ["./admin-list.component.css"],
})
export class AdminListComponent implements OnInit {
  adminList: Array<any>;
  adminId: Array<any> = [];
  admin: Array<any>;
  masterSelected: boolean;
  checkedList: any;
  showTrash: boolean;
  pageNumber: number = 1;
  pageSize: number = 10;
  totalCount: any;
  constructor(
    private superAdminService: SuperAdminService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) {
    setTimeout(() => {
      // tslint:disable-next-line: only-arrow-functions
      $(function () {
        $("#example1").DataTable({
          paging: false,
          columnDefs: [{ orderable: false, targets: [-1, 6] }],
        });
      });
    }, 2000);
  }

  filters: Filter = {
    sortOrder: "",
    sortField: "",
    pageNumber: 1,
    pageSize: 10,
    searchQuery: "",
    filterBy: "",
  };
  ngOnInit() {
    this.getAdminList();
  }

  getAdminList() {
    this.spinner.show();

    this.superAdminService.getAllAdminList(this.filters).subscribe((res) => {
      this.totalCount = res.data.totalCount;
      this.adminList = res.data.dataList.map(function (x) {
        return {
          adminId: x.adminId,
          applicationStatus: x.applicationStatus,
          createdDate: x.createdDate,
          dialCode: x.dialCode,
          email: x.email,
          firstName: x.firstName,
          gstNumber: x.gstNumber,
          isDeleted: x.isDeleted,
          lastName: x.lastName,
          phoneNumber: x.phoneNumber,
          isSelected: false,
        };
      });
      this.spinner.hide();
    });
  }

  details(adminId) {
    this.router.navigate(["home/adminList", adminId]);
  }

  checkUncheckAll() {
    for (var i = 0; i < this.adminList.length; i++) {
      this.adminList[i].isSelected = this.masterSelected;
    }
    this.getCheckedItemList();
  }
  isAllSelected() {
    this.masterSelected = this.adminList.every(function (item: any) {
      return item.isSelected == true;
    });
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this.showTrash = true;
    this.checkedList = [];
    for (var i = 0; i < this.adminList.length; i++) {
      if (this.adminList[i].isSelected)
        this.checkedList.push({ adminId: this.adminList[i].adminId });
    }

    if (this.checkedList.length === 0) {
      this.showTrash = false;
    } else {
      this.showTrash = true;
    }
  }

  deleteIDs() {
    var data = {
      deleteAdminVM: this.checkedList,
    };

    Swal.fire({
      title: "Delete Admin",
      html: "Are you sure you want to delete this admin ?",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
    }).then((result) => {
      if (result.isConfirmed) {
        this.spinner.show();

        this.superAdminService.deleteAdmin(data).subscribe((res) => {
          if (res.status) {
            this.toastr.success(res.message);
            this.getAdminList();
            this.spinner.hide();
            this.showTrash = false;
          } else {
            this.toastr.success(res.message);
            this.spinner.hide();
            return;
          }
        });
      }
    });
  }
}
