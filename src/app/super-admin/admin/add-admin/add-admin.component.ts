import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { SuperAdminService } from "../../super-admin.service";

@Component({
  selector: "app-add-admin",
  templateUrl: "./add-admin.component.html",
  styleUrls: ["./add-admin.component.css"],
})
export class AddAdminComponent implements OnInit {
  addAdminForm: FormGroup;
  count: any = 0;
  submitted: boolean;
  totalRow: any;
  filterEmail: Array<any> = [];

  @ViewChild("backToTop")
  backToTop: ElementRef;

  constructor(
    private fb: FormBuilder,
    private superAdminService: SuperAdminService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {
    const items = [];
    items.push(
      this.fb.group({
        emailId: [
          "",
          [
            Validators.required,
            Validators.email,
            Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"),
          ],
        ],
      })
    );

    this.addAdminForm = this.fb.group({
      addAdminVM: this.fb.array(items, [Validators.required]),
    });
  }

  get addAdminVM(): FormArray {
    return this.addAdminForm.get("addAdminVM") as FormArray;
  }
  ngOnInit() {}
  addRow() {
    const details = this.addAdminForm.get("addAdminVM") as FormArray;
    details.push(this.createItem());
    this.count += 1;
  }

  createItem(): FormGroup {
    return this.fb.group({
      emailId: [
        "",
        [
          Validators.required,
          Validators.email,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"),
        ],
      ],
    });
  }

  doSubmit() {
    this.submitted = true;

    if (this.addAdminForm.invalid) {
      return;
    }
    this.filterEmail = this.addAdminForm.value.addAdminVM.filter(
      (x) => x.emailId != null && x.emailId != ""
    );

    var data = {
      addAdminVM: this.filterEmail,
    };

    this.backToTop.nativeElement.scrollIntoView({ behavior: "smooth" });
  }

  deleteId(index) {
    this.count -= 1;
    const details = this.addAdminForm.get("addAdminVM") as FormArray;
    if (details != null) {
      this.totalRow = details.value.length;
    }
    if (this.totalRow > 1) {
      details.removeAt(index);
    } else {
      this.toastr.warning("One Email is mandatory");
      return false;
    }
  }
}
