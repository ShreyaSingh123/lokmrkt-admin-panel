import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { Filter } from "src/app/shared/interfaces/filter";
import { SuperAdminService } from "../../super-admin.service";
declare var $;

@Component({
  selector: "app-user-list",
  templateUrl: "./user-list.component.html",
  styleUrls: ["./user-list.component.css"],
})
export class UserListComponent implements OnInit {
  List: Array<any>;
  totalCount: any;
  filters: Filter = {
    sortOrder: "",
    sortField: "",
    pageNumber: 1,
    pageSize: 10,
    searchQuery: "",
    filterBy: "",
  };
  constructor(
    private superAdminService: SuperAdminService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {
    setTimeout(() => {
      // tslint:disable-next-line: only-arrow-functions
      $(function () {
        $("#example1").DataTable({
          paging: false,
          columnDefs: [{ orderable: false, targets: [-1] }],
        });
      });
    }, 2000);
  }

  ngOnInit() {
    this.spinner.show();
    this.superAdminService.userList(this.filters).subscribe((res) => {
      this.totalCount = res.data.totalCount;
      this.List = res.data.dataList;
      this.spinner.hide();
    });
  }

  deleteID(userId) {
    this.router.navigate(["home/userList", userId]);
  }
}
