import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, FormControl } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { environment } from "src/environments/environment";
import { SuperAdminService } from "../../super-admin.service";

@Component({
  selector: "app-user-details",
  templateUrl: "./user-details.component.html",
  styleUrls: ["./user-details.component.css"],
})
export class UserDetailsComponent implements OnInit {
  basicInfoForm: FormGroup;
  userDetails: any;
  userId: any;
  isLoaded = false;
  baseUrl = environment.rootPathUrl;
  profilePic: any;
  disabled: boolean = true;
  defaultPic = environment.adminDefaultPic;

  constructor(
    private superAdminService: SuperAdminService,
    private formBuilder: FormBuilder,

    private route: ActivatedRoute,
    private toasterService: ToastrService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.basicInfoForm = this.formBuilder.group({
      name: new FormControl({ value: "", disabled: true }),

      email: new FormControl({ value: "", disabled: true }),
      phoneNumber: new FormControl({ value: "", disabled: true }),
      dateOfBirth: new FormControl({ value: "", disabled: true }),
      applicationStatus: new FormControl({ value: "", disabled: true }),
    });
    this.userId = this.route.snapshot.paramMap.get("id");

    this.getUserDetails(this.userId);
  }

  getUserDetails(id: any) {
    this.spinner.show();
    this.superAdminService.getUserInfo(id).subscribe((res) => {
      if (res.status) {
        this.userDetails = res.data;

        this.profilePic = this.baseUrl + res.data.profilePic;
        if (res.data.profilePic != "") {
          this.profilePic = res.data.profilePic;
        } else {
          this.profilePic = this.defaultPic;
        }

        this.basicInfoForm.controls["name"].setValue(this.userDetails.name);

        this.basicInfoForm.controls["phoneNumber"].setValue(
          this.userDetails.phoneNumber
        );
        this.basicInfoForm.controls["email"].setValue(this.userDetails.email);
        this.basicInfoForm.controls["applicationStatus"].setValue(
          this.userDetails.applicationStatus
        );

        this.isLoaded = true;

        this.spinner.hide();
      } else {
        this.spinner.hide();
        this.toasterService.error(res.message);
        this.spinner.hide();
      }
    });
  }
}
