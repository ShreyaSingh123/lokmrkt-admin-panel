import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { Filter } from "src/app/shared/interfaces/filter";
import { SuperAdminService } from "../../super-admin.service";
declare var $;
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { TemplateRef } from "@angular/core";

@Component({
  selector: "app-assign-vendor",
  templateUrl: "./assign-vendor.component.html",
  styleUrls: ["./assign-vendor.component.css"],
})
export class AssignVendorComponent implements OnInit {
  adminList: any;
  vendorsList: Array<any>;
  masterSelected: boolean;
  checkedList: Array<any> = [];
  checkedAdminList: Array<any> = [];
  updatedVendorList: any;
  assignVendorList: Array<any>;
  adminId: any;
  totalCount: any;
  showButton: boolean;
  modalRef: BsModalRef;
  config = {
    ignoreBackdropClick: true,
  };
  constructor(
    private superAdminService: SuperAdminService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private modalService: BsModalService
  ) {
    setTimeout(() => {
      // tslint:disable-next-line: only-arrow-functions
      $(function () {
        $("#example1").DataTable({
          paging: false,
          columnDefs: [{ orderable: false, targets: [0] }],
        });
      });
    }, 2000);
  }
  filters: Filter = {
    sortOrder: "",
    sortField: "",
    pageNumber: 1,
    pageSize: 10,
    searchQuery: "",
    filterBy: "",
  };
  ngOnInit() {
    this.getAdminList();
  }

  getAdminList() {
    this.spinner.show();
    this.superAdminService.getAllAdminList(this.filters).subscribe((res) => {
      this.totalCount = res.data.totalCount;

      this.adminList = res.data.dataList.map((x) => ({
        ...x,
        isSelected: false,
      }));
      this.spinner.hide();
    });
  }

  selectedAdmin(adminId, template: TemplateRef<any>) {
    this.showButton = false;
    this.adminId = adminId;

    if (adminId) {
      this.spinner.show();
      this.superAdminService.getVendorList(this.filters).subscribe((res) => {
        this.vendorsList = res.data.dataList.filter(
          (x) => x.isAssigned === false
        );

        this.modalRef = this.modalService.show(template, {
          class: "modal-lg",
          ignoreBackdropClick: true,
        });

        this.spinner.hide();
      });
    }
  }

  isAllSelected() {
    this.masterSelected = this.vendorsList.every(function (item: any) {
      return item.isAssigned == true;
    });
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this.checkedList = [];
    for (var i = 0; i < this.vendorsList.length; i++) {
      if (this.vendorsList[i].isAssigned)
        this.checkedList.push(this.vendorsList[i]);
    }
  }
  getVendorView(event, item) {
    this.updatedVendorList = item;

    this.updatedVendorList.permissions.canView = event.target.checked;
    this.assignVendor();
  }
  getVendorEdit(event, item) {
    this.updatedVendorList = item;
    this.updatedVendorList.permissions.canEdit = event.target.checked;
    this.assignVendor();
  }
  getVendorDelete(event, item) {
    this.updatedVendorList = item;

    this.updatedVendorList.permissions.canDelete = event.target.checked;
    this.assignVendor();
  }

  assignVendor() {
    if (this.checkedList.length === 0) {
      this.checkedList.push(this.updatedVendorList);
    } else if (this.updatedVendorList.length != 0) {
      if (
        this.checkedList.some(
          (x) => x.vendorId === this.updatedVendorList.vendorId
        )
      ) {
        var index = this.checkedList.findIndex(
          (item) => item.vendorId === this.updatedVendorList.vendorId
        );
      } else {
        this.checkedList.push(this.updatedVendorList);
      }
    }

    if (this.checkedList.length != 0) {
      this.showButton = true;
    }
  }

  assignIDs() {
    this.assignVendorList = this.checkedList.map((x) => ({
      permissions: x.permissions,

      isAssigned: x.isAssigned,
      vendorId: x.vendorId,
    }));

    var data = {
      adminId: this.adminId,
      assignPermissions: this.assignVendorList,
    };
    this.spinner.show();
    this.superAdminService.assignVendor(data).subscribe((res) => {
      if (res.status) {
        this.spinner.hide();
        this.toastr.success(res.message);
        this.getAdminList();
        this.modalRef.hide();
      } else {
        this.spinner.hide();
        this.toastr.warning(res.message);
      }
    });
  }

  closeModal() {
    this.getAdminList();
    this.modalRef.hide();
  }
}
