import { Component, OnInit, TemplateRef } from "@angular/core";
import { Router } from "@angular/router";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { Filter } from "src/app/shared/interfaces/filter";
import { SuperAdminService } from "../../super-admin.service";
declare var $;

@Component({
  selector: "app-assign-user",
  templateUrl: "./assign-user.component.html",
  styleUrls: ["./assign-user.component.css"],
})
export class AssignUserComponent implements OnInit {
  adminList: any;
  usersList: Array<any>;
  masterSelected: boolean;
  checkedList: Array<any> = [];
  checkedAdminList: Array<any> = [];
  updatedUserList: any;
  assignUserList: Array<any>;
  adminId: any;
  totalCount: any;
  showButton: boolean;
  modalRef: BsModalRef;

  config = {
    ignoreBackdropClick: true,
  };
  constructor(
    private superAdminService: SuperAdminService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private modalService: BsModalService
  ) {
    setTimeout(() => {
      // tslint:disable-next-line: only-arrow-functions
      $(function () {
        $("#example1").DataTable({
          paging: false,
          columnDefs: [{ orderable: false, targets: [0] }],
        });
      });
    }, 2000);
  }
  filters: Filter = {
    sortOrder: "",
    sortField: "",
    pageNumber: 1,
    pageSize: 10,
    searchQuery: "",
    filterBy: "",
  };
  ngOnInit() {
    this.getAdminList();
  }

  getAdminList() {
    this.spinner.show();
    this.superAdminService.getAllAdminList(this.filters).subscribe((res) => {
      this.totalCount = res.data.totalCount;

      this.adminList = res.data.dataList.map(function (x) {
        return {
          ...x,
          isSelected: false,
        };
      });
      this.spinner.hide();
    });
  }

  selectedAdmin(adminId, template: TemplateRef<any>) {
    this.showButton = false;
    this.adminId = adminId;

    if (adminId) {
      this.spinner.show();
      this.superAdminService.userList(this.filters).subscribe((res) => {
        this.usersList = res.data.dataList.filter(
          (x) => x.isAssigned === false
        );

        this.modalRef = this.modalService.show(template, {
          class: "modal-lg",
          ignoreBackdropClick: true,
        });

        this.spinner.hide();
      });
    }
  }

  isAllSelected() {
    this.masterSelected = this.usersList.every(function (item: any) {
      return item.isAssigned == true;
    });
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this.checkedList = [];
    for (var i = 0; i < this.usersList.length; i++) {
      if (this.usersList[i].isAssigned)
        this.checkedList.push(this.usersList[i]);
    }
  }
  getUserView(event, item) {
    this.updatedUserList = item;

    this.updatedUserList.permissions.canView = event.target.checked;
    this.assignUser();
  }
  getUserEdit(event, item) {
    this.updatedUserList = item;
    this.updatedUserList.permissions.canEdit = event.target.checked;
    this.assignUser();
  }
  getUserDelete(event, item) {
    this.updatedUserList = item;

    this.updatedUserList.permissions.canDelete = event.target.checked;
    this.assignUser();
  }

  assignUser() {
    if (this.checkedList.length === 0) {
      this.checkedList.push(this.updatedUserList);
    } else if (this.updatedUserList.length != 0) {
      if (
        this.checkedList.some((x) => x.userId === this.updatedUserList.userId)
      ) {
        var index = this.checkedList.findIndex(
          (item) => item.userId === this.updatedUserList.userId
        );
      } else {
        this.checkedList.push(this.updatedUserList);
      }
    }

    if (this.checkedList.length != 0) {
      this.showButton = true;
    }
  }

  assignIDs() {
    this.assignUserList = this.checkedList.map((x) => ({
      permissions: x.permissions,

      isAssigned: x.isAssigned,
      userId: x.userId,
    }));

    var data = {
      adminId: this.adminId,
      assignPermissions: this.assignUserList,
    };
    this.spinner.show();
    this.superAdminService.assignUser(data).subscribe((res) => {
      if (res.status) {
        this.spinner.hide();
        this.toastr.success(res.message);
        this.getAdminList();
        this.modalRef.hide();
      } else {
        this.spinner.hide();
        this.toastr.warning(res.message);
      }
    });
  }

  closeModal() {
    this.getAdminList();
    this.modalRef.hide();
  }
}
