import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { ApiEndPoint } from "../shared/enums/api-end-point.enum";

@Injectable({
  providedIn: "root",
})
export class SuperAdminService {
  constructor(private http: HttpClient) {}

  addAdmin(data) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.addAdmin,
      data
    );
  }

  getAllAdminList(data: any) {
    return this.http.get<any>(
      environment.apiUrl +
        "/" +
        ApiEndPoint.getAllAdmin +
        "?pageNumber=" +
        data.pageNumber +
        "&pageSize=" +
        data.pageSize +
        "&sortOrder=" +
        data.sortOrder +
        "&sortField=" +
        data.sortField +
        "&searchQuery=" +
        data.searchQuery +
        "&filterBy=" +
        data.filterBy
    );
  }

  userList(data) {
    return this.http.get<any>(
      environment.apiUrl +
        "/" +
        ApiEndPoint.getUserList +
        "?pageNumber=" +
        data.pageNumber +
        "&pageSize=" +
        data.pageSize +
        "&sortOrder=" +
        data.sortOrder +
        "&sortField=" +
        data.sortField +
        "&searchQuery=" +
        data.searchQuery +
        "&filterBy=" +
        data.filterBy
    );
  }
  deleteAdmin(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.deleteAdmin,
      data
    );
  }

  getAdminDetails(id: string) {
    return this.http
      .get<any>(
        environment.apiUrl + "/" + ApiEndPoint.adminDetails + "?AdminId=" + id
      )
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  getUserInfo(id: string) {
    return this.http
      .get<any>(
        environment.apiUrl + "/" + ApiEndPoint.getUserInfo + "?userId=" + id
      )
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  getVendorList(data: any) {
    return this.http.get<any>(
      environment.apiUrl +
        "/" +
        ApiEndPoint.vendorsList +
        "?pageNumber=" +
        data.pageNumber +
        "&pageSize=" +
        data.pageSize +
        "&sortOrder=" +
        data.sortOrder +
        "&sortField=" +
        data.sortField +
        "&searchQuery=" +
        data.searchQuery +
        "&filterBy=" +
        data.filterBy
    );
  }

  getAdminBasedVendor(adminId, pageSize, pageNumber) {
    return this.http.get<any>(
      environment.apiUrl +
        "/" +
        ApiEndPoint.vendorsList +
        "?searchQuery=" +
        adminId +
        "&pageSize=" +
        pageSize +
        "&pageNumber=" +
        pageNumber
    );
  }

  //getProductList
  vendorProductList(pageNumber, pageSize, vendorId) {
    return this.http.get<any>(
      environment.apiUrl +
        "/" +
        ApiEndPoint.productList +
        "?pageNumber=" +
        pageNumber +
        "&pageSize=" +
        pageSize +
        "&vendorId=" +
        vendorId
    );
  }

  //approveProduct
  approveProduct(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.approveProduct,
      data
    );
  }

  assignVendor(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.assignVendor,
      data
    );
  }

  assignUser(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.assignUser,
      data
    );
  }
}
