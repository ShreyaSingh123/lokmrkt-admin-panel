import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorProductDetailsComponent } from './vendor-product-details.component';

describe('VendorProductDetailsComponent', () => {
  let component: VendorProductDetailsComponent;
  let fixture: ComponentFixture<VendorProductDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorProductDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorProductDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
