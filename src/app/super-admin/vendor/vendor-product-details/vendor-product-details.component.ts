import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { SuperAdminService } from "../../super-admin.service";

@Component({
  selector: "app-vendor-product-details",
  templateUrl: "./vendor-product-details.component.html",
  styleUrls: ["./vendor-product-details.component.css"],
})
export class VendorProductDetailsComponent implements OnInit {
  vendorId: any;
  prodcutList: Array<any>;
  masterSelected: boolean;
  checkedList: any;
  ApproveProduct: boolean;
  trueList: Array<any>;
  pageNumber: number = 1;
  pageSize: number = 10;
  constructor(
    private route: ActivatedRoute,
    private superAdminService: SuperAdminService,
    private spiner: NgxSpinnerService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.vendorId = "1d800aa8-3a94-46c1-b26b-9f34c7767e3e"; //this.route.snapshot.paramMap.get("id");

    this.getProductList();
  }

  getProductList() {
    this.superAdminService
      .vendorProductList(this.pageNumber, this.pageSize, this.vendorId)
      .subscribe((res) => {
        this.prodcutList = res.data.dataList.map((x) => ({
          categoryId: x.categoryId,
          categoryName: x.categoryName,
          isProductApproved: false, //x.isProductApproved,
          productId: x.productId,
          productName: x.productName,
          subCategoryId: x.subCategoryId,
          subCategoryName: x.subCategoryName,
          subSubCategoryId: x.subSubCategoryId,
          subSubCategoryName: x.subSubCategoryName,
          vendorId: x.vendorId,
          productApproved: false, //x.isProductApproved,
        }));
      });
  }

  checkUncheckAll() {
    for (var i = 0; i < this.prodcutList.length; i++) {
      this.prodcutList[i].isProductApproved = this.masterSelected;
    }
    this.getCheckedItemList();
  }
  isAllSelected() {
    this.masterSelected = this.prodcutList.every(function (item: any) {
      return item.isProductApproved == true;
    });
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this.checkedList = [];

    for (var i = 0; i < this.prodcutList.length; i++) {
      if (this.prodcutList[i].isProductApproved)
        this.checkedList.push({
          vendorId: this.prodcutList[i].vendorId,
          productId: this.prodcutList[i].productId,
        });
    }

    if (this.checkedList.length === 0) {
      this.ApproveProduct = false;
    } else {
      this.ApproveProduct = true;
    }
  }

  approveProduct() {
    if (this.checkedList.length != 0) {
      var data = {
        productApprovalList: this.checkedList,
      };

      this.spiner.show();

      this.superAdminService.approveProduct(data).subscribe((res) => {
        if (res.status) {
          this.getProductList();
          this.toastr.success(res.message);
          this.spiner.hide();
        } else {
          this.toastr.success(res.message);
          this.spiner.hide();
        }
      });
    }
  }
}
