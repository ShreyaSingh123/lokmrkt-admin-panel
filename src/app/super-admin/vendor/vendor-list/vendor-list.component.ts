import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { Filter } from "src/app/shared/interfaces/filter";
import { SuperAdminService } from "../../super-admin.service";
declare var $;

@Component({
  selector: "app-vendor-list",
  templateUrl: "./vendor-list.component.html",
  styleUrls: ["./vendor-list.component.css"],
})
export class VendorListComponent implements OnInit {
  list: Array<any>;
  filters: Filter = {
    sortOrder: "",
    sortField: "",
    pageNumber: 1,
    pageSize: 10,
    searchQuery: "",
    filterBy: "",
  };
  constructor(
    private superAdminService: SuperAdminService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {
    setTimeout(() => {
      // tslint:disable-next-line: only-arrow-functions
      $(function () {
        $("#example1").DataTable({
          paging: false,
          columnDefs: [{ orderable: false, targets: [-1] }],
        });
      });
    }, 2000);
  }

  ngOnInit() {
    this.spinner.show();
    this.superAdminService.getVendorList(this.filters).subscribe((res) => {
      this.list = res.data.dataList;
      this.spinner.hide();
    });
  }

  // showDetails(vendorId) {
  //   this.router.navigate(["home/vendorList", vendorId]);
  // }

  product(id) {
    this.router.navigate(["home/vendorList", id]);
  }
}
