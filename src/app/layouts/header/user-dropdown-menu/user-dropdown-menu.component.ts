import {
  Component,
  OnInit,
  ViewChild,
  HostListener,
  ElementRef,
  Renderer2,
} from "@angular/core";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { AuthService } from "src/app/shared/services/auth.service";

@Component({
  selector: "app-user-dropdown-menu",
  templateUrl: "./user-dropdown-menu.component.html",
  styleUrls: ["./user-dropdown-menu.component.css"],
})
export class UserDropdownMenuComponent implements OnInit {
  @ViewChild("dropdownMenu", { static: false }) dropdownMenu;
  @HostListener("document:click", ["$event"])
  clickout(event) {
    if (!this.elementRef.nativeElement.contains(event.target)) {
      this.hideDropdownMenu();
    }
  }

  constructor(
    private elementRef: ElementRef,
    private renderer: Renderer2,
    private auth: AuthService,
    private toastr: ToastrService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {}

  toggleDropdownMenu() {
    if (this.dropdownMenu.nativeElement.classList.contains("show")) {
      this.hideDropdownMenu();
    } else {
      this.showDropdownMenu();
    }
  }

  showDropdownMenu() {
    this.renderer.addClass(this.dropdownMenu.nativeElement, "show");
  }

  hideDropdownMenu() {
    this.renderer.removeClass(this.dropdownMenu.nativeElement, "show");
  }

  logout() {
    this.spinner.show();
    this.auth.logout().subscribe((res) => {
      if (res.status) {
        this.toastr.success("Successfully Log-Out");
        this.spinner.hide();
        this.router.navigateByUrl("/login");
      } else {
        this.spinner.hide();
      }
    });
  }
}
