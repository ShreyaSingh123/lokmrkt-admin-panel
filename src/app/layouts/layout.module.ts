import { NgModule } from "@angular/core";
import { MenuComponent } from "./menu/menu.component";
import { FooterComponent } from "./footer/footer.component";
import { HeaderComponent } from "./header/header.component";
import { MessagesDropdownMenuComponent } from "./header/messages-dropdown-menu/messages-dropdown-menu.component";
import { NotificationsDropdownMenuComponent } from "./header/notifications-dropdown-menu/notifications-dropdown-menu.component";
import { UserDropdownMenuComponent } from "./header/user-dropdown-menu/user-dropdown-menu.component";
import { RouterModule } from "@angular/router";
import { LayoutComponent } from "./layout.component";
import { BrowserModule } from "@angular/platform-browser";
import { CommonModule } from "@angular/common";

@NgModule({
  declarations: [
    HeaderComponent,
    MessagesDropdownMenuComponent,
    NotificationsDropdownMenuComponent,
    UserDropdownMenuComponent,
    MenuComponent,
    FooterComponent,
    LayoutComponent,
  ],
  imports: [RouterModule, CommonModule],
  providers: [],
})
export class LayoutModule {}
