import {
  Component,
  OnInit,
  Output,
  ViewChild,
  EventEmitter,
} from "@angular/core";
declare let $: any;

@Component({
  selector: "app-menu",
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.css"],
})
export class MenuComponent implements OnInit {
  roles: any;
  superAdmin: boolean;
  data: any;

  @ViewChild("mainSidebar", { static: false }) mainSidebar;
  @Output() mainSidebarHeight: EventEmitter<any> = new EventEmitter<any>();
  constructor() {}

  ngOnInit() {
    this.roles = JSON.parse(localStorage.getItem("currentUser"));

    if (this.roles.data.roles == "SuperAdmin") {
      this.superAdmin = true;
    }
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngAfterViewInit() {
    $('[data-widget="treeview"]').Treeview("init");
    this.mainSidebarHeight.emit(this.mainSidebar.nativeElement.offsetHeight);
  }
}
