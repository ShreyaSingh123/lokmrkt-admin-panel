import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { environment } from "src/environments/environment";
import { NotFoundComponent } from "./auth/not-found/not-found.component";
const routes: Routes = [
  { path: "", redirectTo: "home", pathMatch: "full" },
  {
    path: "auth",
    loadChildren: () => import("./auth/auth.module").then((m) => m.AuthModule),
  },
  {
    path: "home",
    loadChildren: () => import("./home/home.module").then((m) => m.HomeModule),
  },
  {
    path: "admin",
    loadChildren: () =>
      import("./admin/admin.module").then((m) => m.AdminModule),
  },

  {
    path: "superAdmin",
    loadChildren: () =>
      import("./super-admin/super-admin.module").then(
        (m) => m.SuperAdminModule
      ),
  },
  {
    path: "404",
    component: NotFoundComponent,
  },
  { path: "**", redirectTo: "/404" },
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      // tslint:disable-next-line: max-line-length
      !environment.production
        ? {
            enableTracing: false,
            useHash: true,
            scrollPositionRestoration: "enabled",
          }
        : { scrollPositionRestoration: "enabled", useHash: true }
    ),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
