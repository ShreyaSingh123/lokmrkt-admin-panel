import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BehaviorSubject, Observable } from "rxjs";
import { map } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { Router } from "@angular/router";
import { Login, Register } from "src/app/shared/models";
import { ApiEndPoint } from "../enums/api-end-point.enum";
import { env } from "process";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  headers_object: any;
  private currentUserSubject: BehaviorSubject<Login>;
  public currentUser: Observable<any>;

  constructor(private http: HttpClient, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<Login>(
      JSON.parse(localStorage.getItem("currentUser"))
    );
    this.currentUser = this.currentUserSubject.asObservable();

    this.headers_object = new HttpHeaders().set(
      "Authorization",
      "Bearer " + localStorage.getItem("token")
    );
  }
  public get currentUserValue(): any {
    return this.currentUserSubject.value;
  }

  //  SuperAdmin Login
  superAdminLogin(user: Login) {
    return (
      this.http
        .post<any>(environment.apiUrl + "/" + ApiEndPoint.superAdminlogin, user)
        // tslint:disable-next-line: no-shadowed-variable
        .pipe(
          map((user) => {
            if (user.status) {
              localStorage.setItem("currentUser", JSON.stringify(user));
              this.currentUserSubject.next(user);
            } else {
              this.router.navigateByUrl("/login");
            }
            return user;
          })
        )
    );
  }
  //Admin Login
  adminLogin(user: Login) {
    return (
      this.http
        .post<any>(environment.apiUrl + "/" + ApiEndPoint.adminLogin, user)
        // tslint:disable-next-line: no-shadowed-variable
        .pipe(
          map((user) => {
            if (user.status) {
              localStorage.setItem("currentUser", JSON.stringify(user));
              this.currentUserSubject.next(user);
            } else {
              this.router.navigateByUrl("/login");
            }
            return user;
          })
        )
    );
  }

  // Admin Register
  register(data) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.registerAdmin,
      data
    );
  }

  logout() {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.logout,
      null
    );
  }

  //email-resend
  resendEmailCode(data) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.resendEmailCode,
      data,
      { headers: this.headers_object }
    );
  }

  //verify email
  verifyEmail(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.verifyEmai,
      data,
      { headers: this.headers_object }
    );
  }
}
