import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class SharedService {

    private subject = new Subject<any>();

  sendSharedData(data: any) {
      this.subject.next({ data });
    }

  clearSharedData() {
      this.subject.next();
      // this.subject.forEach(subscription => subscription.unsubscribe());
    }

    getSharedData(): Observable<any> {
        return this.subject.asObservable();
    }
}
