import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class FileUploadService {

    constructor(private http: HttpClient) {

    }

    uploadFile(apiUrl, fileToUpload: File) {
        // tslint:disable-next-line: variable-name
        const _formData = new FormData();
        // _formData.append('file', fileToUpload, fileToUpload.name);
        _formData.append('file', fileToUpload);
        return this.http.post(apiUrl, _formData);
    }

}
