enum VendorApplicationStatus {
  Incomplete = 0,
  Pending,
  Accepted,
  Rejected,
}
