export enum ApiEndPoint {
  // Login  Api EndPoints
  superAdminlogin = "Auth/SuperAdminLogin",
  adminLogin = "Auth/Login",
  registerAdmin = "Auth/Register",
  addAdmin = "Admin/AddAdmin",
  getAllAdmin = "Admin/GetAllAdmin",
  resendEmailCode = "Auth/ResendEmailCode",
  verifyEmai = "Auth/VerifyEmail",
  logout = "Auth/Logout",
  adminDetails = "Admin/GetAdminDetails",
  deleteAdmin = "Admin/DeleteAdmin",

  //user
  getUserList = "Admin/GetUsersList",
  getUserInfo = "Admin/GetUserInfo",

  //vendor
  vendorsList = "Admin/GetVendorsList",
  productList = "Admin/GetProductsList",
  approveProduct = "Admin/ApproveProduct",

  //Assign
  assignVendor = "Admin/AssignVendors",
  assignUser = "Admin/AssignUsers",
}
