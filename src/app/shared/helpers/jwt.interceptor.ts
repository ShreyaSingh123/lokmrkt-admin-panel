import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
} from "@angular/common/http";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { AuthService } from "../services/auth.service";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import Swal from "sweetalert2";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(
    private authService: AuthService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private toaster: ToastrService
  ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const currentUser = this.authService.currentUserValue;
    if (currentUser) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${currentUser.data.accessToken}`,
        },
      });
    }
    return next.handle(request).pipe(
      tap(
        (event) => {
          if (event instanceof HttpResponse) {
          }
        },
        (error) => {
          switch (error.status) {
            case 401: {
              this.spinner.hide();
              Swal.fire({
                title: "",
                text:
                  "Your Session has been expired as you are logged in from another device",
                icon: "error",
                showCancelButton: false,
                confirmButtonColor: "#3085d6",
                allowOutsideClick: false,
                backdrop: `
                rgba(0, 0, 0, 0.8)
                left top
                no-repeat
                  `,
                cancelButtonColor: "#d33",
                confirmButtonText: "Ok",
              }).then((result) => {
                if (result.isConfirmed) {
                  this.authService.logout();
                  localStorage.removeItem("currentUser");
                  this.router.navigate(["login"]);
                }
              });
              break;
            }
            case 500: {
              this.spinner.hide();
              this.toaster.error("Internal Server Error");
              break;
            }
            case 0: {
              this.spinner.hide();
              Swal.fire({
                title: "",
                text: "Please Check Your Internet Connection",
                icon: "error",
                allowOutsideClick: false,
                backdrop: `
                  rgba(0, 0, 0, 0.8)
                  left top
                  no-repeat
                    `,
                cancelButtonColor: "#d33",
                confirmButtonText: "Ok",
              }).then((result) => {
                return;
              });

              break;
            }
          }
        }
      )
    );
  }
}
