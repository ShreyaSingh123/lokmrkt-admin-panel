export class Register {
  email: string;
  password: string;
  confirmPassword: string;
  deviceId: string;
  deviceType: string;
}
