export class Speciality {
  id: number;
  name: string;
  image: File;
  isActive: boolean;
}
