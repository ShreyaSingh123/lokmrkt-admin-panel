export class FilterListModel {
  sortOrder: string;
  sortField: string;
  pageNumber: number;
  pageSize: number;
  searchQuery: string;
  filterBy: any;
}
