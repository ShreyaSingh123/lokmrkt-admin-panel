export class ResetPassword {
  email: string;
  otp: number;
  newPassword: string;
}
