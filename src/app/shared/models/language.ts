export class Language {
  id: number;
  name: string;
  isActive: boolean;
}
