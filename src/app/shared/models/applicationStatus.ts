export class DoctorApplicationStatus {
  userId: string;
  applicationStatus: number;
}
