export class Degree {
  id: number;
  name: string;
  isActive: boolean;
}
