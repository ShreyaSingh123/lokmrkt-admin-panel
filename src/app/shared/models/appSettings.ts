export class ApplicationSettingInfo {
  id: number;
  aboutUs: string;
  privacyPolicy: string;
  termsConditions: string;
}
