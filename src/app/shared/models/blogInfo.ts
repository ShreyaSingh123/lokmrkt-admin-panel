export class BlogInfo {
  id: string;
  title: string;
  description: string;
  blogImagePath: string;
  doctorName: string;
  isAdminApproved: boolean;
}
