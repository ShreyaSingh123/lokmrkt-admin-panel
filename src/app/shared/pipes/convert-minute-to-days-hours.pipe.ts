import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ConvertMinToDayHours',
  pure: true
})

export class ConvertMinToDayHoursPipe implements PipeTransform {
  transform(value: number): any {
     if (!isNaN(value)) {
        if (Math.floor(value / 60) <= 1) {
        return Math.floor(value) + ' Minutes';
      } else if (Math.floor(value / 60) > 1 && Math.floor(value / 60) <= 24) {
        return Math.floor(value / 60) + ' Hour(s)';
      } else {
        const seconds = value * 60;
        const days = Math.floor(seconds / 86400);
        return days + ' day(s)';
      }
    }
     return;
  }
}
