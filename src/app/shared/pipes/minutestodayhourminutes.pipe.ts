import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dayhoursMinutes',
  pure: true
})
export class DayHoursMinutesPipe implements PipeTransform {
  transform(minutes: number, format = 'short', includeDays = true): any {
    let formatted = '';
    // set minutes to seconds
    let seconds = minutes * 60;
    // calculate (and subtract) whole days
    let days = 0;
    if (includeDays) {
      days = Math.floor(seconds / 86400);
      seconds -= days * 86400;
      formatted = `${days} days `;
    }
    // calculate (and subtract) whole hours
    const hours = Math.floor(seconds / 3600) % 24;
    seconds -= hours * 3600;
    // calculate (and subtract) whole minutes
    const min = Math.floor(seconds / 60) % 60;
    formatted += `${hours}h ${min}m`;
    if ('long' === format) {
      formatted = formatted.replace('d', ' days');
      formatted = formatted.replace('h', ' hours');
      formatted = formatted.replace('m', ' minutes');
    }
    return formatted;
  }
}
