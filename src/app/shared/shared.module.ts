import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { OnlyNumberDirective } from "./directives/only-number.directive";

@NgModule({
  declarations: [],
  imports: [CommonModule, OnlyNumberDirective],
  exports: [OnlyNumberDirective],
})
export class SharedModule {}
